package com.example.cp.car_pooling_app;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> src;
    private ArrayList<String> dest;
    private ArrayList<String> cost;
    private ArrayList<String> date;
    private ArrayList<String> v_num;
    private ArrayList<String> d_name;
    private ArrayList<String> d_con;

    public HistoryAdapter(@NonNull Activity context, ArrayList<String> src, ArrayList<String> dest, ArrayList<String> cost, ArrayList<String> date, ArrayList<String> v_num, ArrayList<String> d_name, ArrayList<String> d_con) {
        super(context, R.layout.history_item,src);
        this.context = context;
        this.src = src;
        this.dest = dest;
        this.cost = cost;
        this.date = date;
        this.v_num = v_num;
        this.d_name = d_name;
        this.d_con = d_con;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.history_item, null, true);

        TextView txtSrc = (TextView) rowView.findViewById(R.id.txtHSource);
        TextView txtDest = (TextView) rowView.findViewById(R.id.txtHDest);
        TextView txtCost = (TextView) rowView.findViewById(R.id.txtHCost);
        TextView txtDate = (TextView) rowView.findViewById(R.id.txtHDate);
        TextView txtVNum = (TextView) rowView.findViewById(R.id.txtHVeh);
        TextView txtDName = (TextView) rowView.findViewById(R.id.txtHDName);
        TextView txtDCon = (TextView) rowView.findViewById(R.id.txtHDCon);

        if (!src.get(0).equals(null)) {
            txtSrc.setText(src.get(position));
            txtDest.setText(dest.get(position));
            txtCost.setText(cost.get(position));
            txtDate.setText(date.get(position).substring(0, 8));
            txtVNum.setText(v_num.get(position));
            txtDName.setText(d_name.get(position));
            txtDCon.setText(d_con.get(position));
        }

        return rowView;
    }
}
